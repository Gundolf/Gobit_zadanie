﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace Gobit_Zadanie
{
    class Worker
    {
        public string Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string Position { get; set; }

        public Worker(string id, string firstname, string lastname, string age, string sex, string position)
        {
            this.Id = id;
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Age = age;
            this.Sex = sex;
            this.Position = position;
        }
    }
    class Positions
    {
        public string Id { get; set; }
        public string Position { get; set; }

        public Positions(string id, string position)
        {
            this.Id = id;
            this.Position = position;
        }
    }
    class Logs
    {
        public string IdLogs { get; set; }
        public string IdWorkers { get; set; }
        public string Operacja { get; set; }
        public string Data { get; set; }

        public Logs(string idLogs, string idWorkers, string operacja, string data)
        {
            this.IdLogs = idLogs;
            this.IdWorkers = idWorkers;
            this.Operacja = operacja;
            this.Data = data;
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static int editId = -1;

        public MainWindow()
        {
            InitializeComponent();
            FillDataGrid();
            FillLogsGrid();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void FillDataGrid()
        {
            MySqlConnection connect;
            string MyConnectString;
            MyConnectString = "server=mariadb2.iq.pl; ;Database=gobit_gobit_test; uid=gobit_gobit_test ;pwd=hl9hDFv9DTnrtTdv5Zwp;";
            //MyConnectString = "server=localhost; ;Database=gobit_test; uid=root ;pwd=; port=3307";

            try
            {
                connect = new MySqlConnection();
                connect.ConnectionString = MyConnectString;
                connect.Open();
                if (connect.State == ConnectionState.Open)
                {
                    string query = "SELECT * FROM worker";
                    string query2 = "SELECT * FROM position";

                    //Create a list to store the result
                    List<string>[] list = new List<string>[6];
                    list[0] = new List<string>();
                    list[1] = new List<string>();
                    list[2] = new List<string>();
                    list[3] = new List<string>();
                    list[4] = new List<string>();
                    list[5] = new List<string>();

                    List<string>[] list2 = new List<string>[2];
                    list2[0] = new List<string>();
                    list2[1] = new List<string>();

                    //Create Command
                    MySqlCommand cmd = new MySqlCommand(query, connect);
                    //Create a data reader and Execute the command
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    //Read the data and store them in the list
                    while (dataReader.Read())
                    {
                        list[0].Add(dataReader["id"] + "");
                        list[1].Add(dataReader["firstname"] + "");
                        list[2].Add(dataReader["lastname"] + "");
                        list[3].Add(dataReader["age"] + "");
                        list[4].Add(dataReader["sex"] + "");
                        list[5].Add(dataReader["position"] + "");
                    }
                    //close Data Reader
                    dataReader.Close();

                    //Create Command
                    MySqlCommand cmd2 = new MySqlCommand(query2, connect);
                    //Create a data reader and Execute the command
                    MySqlDataReader dataReader2 = cmd2.ExecuteReader();

                    //Read the data and store them in the list
                    while (dataReader2.Read())
                    {
                        list2[0].Add(dataReader2["id"] + "");
                        list2[1].Add(dataReader2["name"] + "");
                    }
                    //close Data Reader
                    dataReader.Close();

                    int rows = list[0].Count;

                    for (int i = 0; i < rows; i++)
                    {
                        if (list[4][i].ToString() == "2")
                        {
                            list[4][i] = "Mezczyzna";
                        }
                        else
                        {
                            list[4][i] = "Kobieta";
                        }
                    }

                    var positions = new List<Positions>();
                    int rows2 = list2[0].Count;
                    for (int i = 0; i < rows2; i++)
                    {
                        positions.Add(new Positions(list2[0][i], list2[1][i]));
                    }

                    for (int i = 0; i < rows; i++)
                    {
                        int temp = Convert.ToInt16(list[5][i].ToString())-1;
                        list[5][i] = positions[temp].Position;
                    }


                    var workers = new List<Worker>();
                    for (int i = 0; i < rows; i++)
                    {
                        workers.Add(new Worker(list[0][i], list[1][i], list[2][i], list[3][i], list[4][i], list[5][i]));
                    }

                    dataGrid1.ItemsSource = workers;
                }
                connect.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillLogsGrid()
        {
            MySqlConnection connect;
            string MyConnectString;
            MyConnectString = "server=mariadb2.iq.pl; ;Database=gobit_gobit_test; uid=gobit_gobit_test ;pwd=hl9hDFv9DTnrtTdv5Zwp;";
            //MyConnectString = "server=localhost; ;Database=gobit_test; uid=root ;pwd=; port=3307";

            try
            {
                connect = new MySqlConnection();
                connect.ConnectionString = MyConnectString;
                connect.Open();

                string query = "SELECT * FROM logs";

                List<string>[] list = new List<string>[4];
                list[0] = new List<string>();
                list[1] = new List<string>();
                list[2] = new List<string>();
                list[3] = new List<string>();

                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connect);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list[0].Add(dataReader["idlogs"] + "");
                    list[1].Add(dataReader["idworker"] + "");
                    list[2].Add(dataReader["operacja"] + "");
                    list[3].Add(dataReader["data"] + "");
                }
                var logs = new List<Logs>();
                int rows = list[0].Count;

                for (int i = 0; i < rows; i++)
                {
                    logs.Add(new Logs(list[0][i], list[1][i], list[2][i], list[3][i]));
                }
                connect.Close();
                logsGrid.ItemsSource = logs;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddWorker(string name, string lastname, int age, int sex, int position)
        {
            MySqlConnection connect;
            string MyConnectString;
            MyConnectString = "server=mariadb2.iq.pl; ;Database=gobit_gobit_test; uid=gobit_gobit_test ;pwd=hl9hDFv9DTnrtTdv5Zwp;";
            //MyConnectString = "server=localhost; ;Database=gobit_test; uid=root ;pwd=; port=3307";

            try
            {
                connect = new MySqlConnection();
                connect.ConnectionString = MyConnectString;
                connect.Open();

                string checkId = "SELECT * FROM worker ORDER BY id DESC LIMIT 1";
                MySqlCommand cmd2 = new MySqlCommand(checkId, connect);
                MySqlDataReader reader = cmd2.ExecuteReader();
                List<string> newid = new List<string>();
                while (reader.Read())
                {
                    newid.Add(reader["id"] + "");
                }
                int id = Convert.ToInt16(newid[0].ToString())+1;
                reader.Close();
                string query = string.Format("INSERT INTO worker(id, firstname, lastname, age, sex, position)" +
                    "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')", id, name, lastname, age, sex, position);
                MySqlCommand cmd = new MySqlCommand(query, connect);
                cmd.ExecuteScalar();
                connect.Close();
                RegisterLog(id, "dodanie");
                FillDataGrid();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadWorker(int id)
        {
            MySqlConnection connect;
            string MyConnectString;
            MyConnectString = "server=mariadb2.iq.pl; ;Database=gobit_gobit_test; uid=gobit_gobit_test ;pwd=hl9hDFv9DTnrtTdv5Zwp;";
            //MyConnectString = "server=localhost; ;Database=gobit_test; uid=root ;pwd=; port=3307";

            try
            {
                connect = new MySqlConnection();
                connect.ConnectionString = MyConnectString;
                connect.Open();
                string query = string.Format("SELECT * FROM worker WHERE id = {0}", id);
                MySqlCommand cmd = new MySqlCommand(query, connect);
                MySqlDataReader reader = cmd.ExecuteReader();
                List<string> editWorker = new List<string>();
                while (reader.Read())
                {
                    editWorker.Add(reader["id"] + "");
                    editWorker.Add(reader["firstname"] + "");
                    editWorker.Add(reader["lastname"] + "");
                    editWorker.Add(reader["age"] + "");
                    editWorker.Add(reader["sex"] + "");
                    editWorker.Add(reader["position"] + "");
                }
                connect.Close();
                nameBox.Text = editWorker[1];
                lastnameBox.Text = editWorker[2];
                ageBox.Text = editWorker[3];
                sexCombo.SelectedIndex = Convert.ToInt16(editWorker[4])-1;
                positionnCombo.SelectedIndex = Convert.ToInt16(editWorker[5]) - 1;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EditWorker(string name, string lastname, int age, int sex, int position)
        {
            MySqlConnection connect;
            string MyConnectString;
            MyConnectString = "server=mariadb2.iq.pl; ;Database=gobit_gobit_test; uid=gobit_gobit_test ;pwd=hl9hDFv9DTnrtTdv5Zwp;";
            //MyConnectString = "server=localhost; ;Database=gobit_test; uid=root ;pwd=; port=3307";

            try
            {
                connect = new MySqlConnection();
                connect.ConnectionString = MyConnectString;
                connect.Open();
                string query = string.Format("UPDATE worker SET " +
                    "firstname = '{1}', lastname = '{2}', age = '{3}', sex = '{4}', position = '{5}' WHERE id = {0}", editId, name, lastname, age, sex, position);
                MySqlCommand cmd = new MySqlCommand(query, connect);
                cmd.ExecuteScalar();
                connect.Close();
                RegisterLog(editId, "edycja");
                FillDataGrid();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DeleteWorker(int id)
        {
            MySqlConnection connect;
            string MyConnectString;
            MyConnectString = "server=mariadb2.iq.pl; ;Database=gobit_gobit_test; uid=gobit_gobit_test ;pwd=hl9hDFv9DTnrtTdv5Zwp;";
            //MyConnectString = "server=localhost; ;Database=gobit_test; uid=root ;pwd=; port=3307";

            try
            {
                connect = new MySqlConnection();
                connect.ConnectionString = MyConnectString;
                connect.Open();

                string query = string.Format("DELETE FROM worker WHERE id = '{0}'", id);
                MySqlCommand cmd = new MySqlCommand(query, connect);
                cmd.ExecuteScalar();
                connect.Close();
                RegisterLog(id, "usuniecie");
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RegisterLog(int id, string action)
        {
            MySqlConnection connect;
            string MyConnectString;
            MyConnectString = "server=mariadb2.iq.pl; ;Database=gobit_gobit_test; uid=gobit_gobit_test ;pwd=hl9hDFv9DTnrtTdv5Zwp;";
            //MyConnectString = "server=localhost; ;Database=gobit_test; uid=root ;pwd=; port=3307";

            try
            {
                connect = new MySqlConnection();
                connect.ConnectionString = MyConnectString;
                connect.Open();
                DateTime date = DateTime.Now;
                string query = string.Format("INSERT INTO logs(idworker, operacja, data)" +
                    "VALUES ('{0}', '{1}', '{2}')", id, action, date);
                MySqlCommand cmd = new MySqlCommand(query, connect);
                cmd.ExecuteScalar();
                connect.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            FillDataGrid();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            string name = nameBox.Text;
            string lastname = lastnameBox.Text;
            int age = Convert.ToInt16(ageBox.Text);
            int sex = sexCombo.SelectedIndex+1;
            int position = positionnCombo.SelectedIndex+1;

            AddWorker(name, lastname, age, sex, position);
        }

        private void delButton_Click(object sender, RoutedEventArgs e)
        {
            int id = Convert.ToInt16(delIdBox.Text);
            DeleteWorker(id);
            FillDataGrid();
        }

        private void loadWorkerButton_Click(object sender, RoutedEventArgs e)
        {
            int id = Convert.ToInt16(editIdBox.Text);
            string editFormat = string.Format("Edytuj id: {0}", id);
            editButton.Content = editFormat;
            editId = id;
            LoadWorker(id);
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            string name = nameBox.Text;
            string lastname = lastnameBox.Text;
            int age = Convert.ToInt16(ageBox.Text);
            int sex = sexCombo.SelectedIndex + 1;
            int position = positionnCombo.SelectedIndex + 1;
            EditWorker(name, lastname, age, sex, position);
        }

        private void logRefresh_Click(object sender, RoutedEventArgs e)
        {
            FillLogsGrid();
        }
    }
}
